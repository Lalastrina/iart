package logic.weka;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class WekaData {

	private Instances trainData;
	private Instances testData;
	private String errors;
	
	public WekaData() {
		errors = "";
	}

	/**
	 * @return the trainData
	 */
	public Instances getTrainData() {
		return trainData;
	}

	/**
	 * Sets train data contents. Uses weka's data extraction tools.
	 * @param filePath the file path
	 */
	public void setTrainData(String filePath) {
		 DataSource source = null;

		try {
			source = new DataSource(filePath);
		} catch (Exception e) {
			//e.printStackTrace();
			addError("Couldn't open file. Is it in the correct format?");
			return;
		}

		try {
			trainData = source.getDataSet();
		} catch (Exception e) {
			//e.printStackTrace();
			addError("Couldn't extract data set from source. Is it in the correct format?");
			return;			
		}

		if(trainData == null) {
			addError("Couldn't extract data set from source. Is it in the correct format?");
			return;
		}
		
		if(errors.isEmpty()) {
			 // Make the last attribute be the class
			if (trainData.classIndex() == -1)
			 trainData.setClassIndex(trainData.numAttributes() - 1);
		}
	}
	
	public void resetTrainData() {
		trainData = null;
	}
	
	public void resetTestData() {
		testData = null;
	}
	
	/**
	 * @return the testData
	 */
	public Instances getTestData() {
		return testData;
	}

	/**
	 * Sets test data contents. Uses weka's data extraction tools.
	 * @param filePath the file path
	 */
	public void setTestData(String filePath) {
		 DataSource source = null;

		try {
			source = new DataSource(filePath);
		} catch (Exception e) {
			//e.printStackTrace();
			addError("Couldn't open file. Is it in the correct format?");
			System.out.println(e.getLocalizedMessage());
			return;
		}

		try {
			testData = source.getDataSet();
		} catch (Exception e) {
			//e.printStackTrace();
			addError("Couldn't extract data set from source. Is it in the correct format?");
			return;
		}

		if(testData == null) {
			addError("Couldn't extract data set from source. Is it in the correct format?");
			return;
		}		
		
		if(errors.isEmpty()) {
			 // Make the last attribute be the class
			if(trainData != null) {
				testData.setClassIndex(trainData.numAttributes() - 1);
			}
			else {
				if (testData.classIndex() == -1)
					testData.setClassIndex(testData.numAttributes() - 1);
			}

		}
	}
	
	/**
	 * Cleans errors string
	 */
	public void cleanErrors() {
		errors = null;
	}
	
	/**
	 * Adds an error to the erros string
	 * @param error
	 */
	public void addError(String error) {
		errors = error;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}
}
