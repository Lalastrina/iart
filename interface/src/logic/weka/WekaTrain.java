package logic.weka;

import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;

public class WekaTrain {

	private Classifier j48;
	private Evaluation evaluation;
	
	private boolean pruned;
	private String errors;
	private int numFolds;
	private long seed;
	
	private long trainTime;
	private String confidence; 
	
	public WekaTrain(boolean pruned) {
		this.setPruned(pruned);
		this.setJ48(new J48());
		//this.setEvaluation(new Evaluation());
		this.setErrors("");
		this.setNumFolds(2);
		this.setConfidence("0.25");
	}

	public void updateOptions(boolean pruned, int numFolds, String confidence) {
		this.pruned = pruned;
		this.numFolds = numFolds;
		this.setConfidence(confidence);
	}
	
	public boolean loadOptions() {
		String[] options;
		
		if(!pruned) {
			options = new String[1];
			options[0] = "-U";// unpruned tree
		}
		else {
			options = new String[2];
			options[0] = "-C";
			options[1] = confidence;//pruned tree with 0.25 confidence
		}		
		
		try {
			j48.setOptions(options);// set the options
		} catch (Exception e) {
			//e.printStackTrace();
			return false;
		} 
		
		return true;
	}
	/**
	 * Trains using data.
	 * @param data
	 */
	public boolean train(Instances data) {
		 
		loadOptions();
		
		long now = System.currentTimeMillis();//System.nanoTime();
		
		try {
			j48.buildClassifier(data);// build classifier
		} catch (Exception e) {
			//e.printStackTrace();
			this.setErrors("Error building classifier:\n"+e.getLocalizedMessage());
			return false;
		}   
		
		this.setTrainTime(System.currentTimeMillis() - now);//System.nanoTime() - now);
		
		return true;
	}
	
	/**
	 * Performs both train and test on the same data
	 * @param data
	 * @return true if all went well
	 * @return false otherwise
	 */
	public boolean crossValidation(Instances data) {
		loadOptions();
		 seed = System.currentTimeMillis();
		 Random rand = new Random(seed);   // create seeded number generator
		 Instances randData = new Instances(data);   // create copy of original data
		 randData.randomize(rand);         // randomize data with number generator
		 if (randData.classAttribute().isNominal())
		      randData.stratify(numFolds);
		 
		 evaluation = null;		 
		 try {
			evaluation = new Evaluation(randData);
		 } catch (Exception e) {
			//e.printStackTrace();
			 this.setErrors("Couldn't perform evaluation.");
			 return false;
		 }

		 for (int n = 0; n < numFolds; n++) {
			 Instances train = randData.trainCV(numFolds, n);
			 Instances test = randData.testCV(numFolds, n);
			 
			try {
				long now = System.currentTimeMillis();
				j48.buildClassifier(train);
				this.setTrainTime(System.currentTimeMillis() - now);
				evaluation.evaluateModel(j48, test);
			} catch (Exception e) {
				//e.printStackTrace();
				this.setErrors("Error performing cross validation:\n"+e.getLocalizedMessage());
				return false;
			}

		}
		 return true;
		 
	}
	
	/**
	 * Performs test on test data, based on the train data
	 * @param dataTest
	 * @param dataTrain
	 * @return true if all went well
	 * @return false otherwise
	 */
	public boolean test(Instances dataTest, Instances dataTrain) {
		try {
			evaluation = new Evaluation(dataTrain);
		} catch (Exception e) {
			//e.printStackTrace();
			this.setErrors("Error generation Evaluation from train data:\n"+e.getLocalizedMessage());
			return false;
		}
		
		try {
			evaluation.evaluateModel(j48, dataTest);
		} catch (Exception e) {
			//e.printStackTrace();
			this.setErrors("Error evaluating test data:\n"+e.getLocalizedMessage());
			return false;
		}
		
		return true;
	}
	
	/**
	 * Returns the tree in dotty format, using J48's graph() function
	 * @return the dotty string if success
	 * @return null if it fails
	 */
	public String getTreeInDottyFormat(){
		
		try {
			return ((J48)getJ48()).graph();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @return the j48
	 */
	public Classifier getJ48() {
		return j48;
	}

	/**
	 * @param j48 the j48 to set
	 */
	public void setJ48(J48 j48) {
		this.j48 = j48;
	}

	/**
	 * @return the pruned
	 */
	public boolean isPruned() {
		return pruned;
	}

	/**
	 * @param pruned the pruned to set
	 */
	public void setPruned(boolean pruned) {
		this.pruned = pruned;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}

	/**
	 * @return the confidence
	 */
	public String getConfidence() {
		return confidence;
	}

	/**
	 * @param confidence the confidence to set
	 */
	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}

	/**
	 * @return the trainTime
	 */
	public long getTrainTime() {
		
		return trainTime;
	}

	/**
	 * @param trainTime the trainTime to set
	 */
	public void setTrainTime(long trainTime) {
		this.trainTime = trainTime;//(double)trainTime/1000000000.0;
	}
	
	/**
	 * @return the numFolds
	 */
	public int getNumFolds() {
		return numFolds;
	}

	/**
	 * @param numFolds the numFolds to set
	 */
	public void setNumFolds(int numFolds) {
		this.numFolds = numFolds;
	}

	/**
	 * @return the seed
	 */
	public long getSeed() {
		return seed;
	}

	/**
	 * @param seed the seed to set
	 */
	public void setSeed(long seed) {
		this.seed = seed;
	}

	/**
	 * @return the evaluation
	 */
	public Evaluation getEvaluation() {
		return evaluation;
	}

	/**
	 * @param evaluation the evaluation to set
	 */
	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}

	public double getNumberOfNodes() {
		return ((J48)this.j48).measureTreeSize();
	}
	
	public double getNumberOfLeaves() {
		return ((J48)this.j48).measureNumLeaves();
	}

}