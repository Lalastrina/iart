package logic.nonweka;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import utils.Parser;
import logic.nonweka.Node;

public class C45 {

	private static final double CONFIDENCE = 0.50;
	
	private DecisionTree tree;
	private ArrayList<ClassifiedInstance> testData;
	private boolean withPruning;
	private boolean withInfoRatio;
	private String errors = "";
	private long trainTime;
	
	
	/**
	 * Constructor. Initializes the tree by adding the root node to it.
	 * Calculates the entropy for the root (the initial entropy of the classifications).
	 * @param classes - the list of classes read from the file
	 * @param attributes - the list of attributes read from the file
	 */
	public C45(ArrayList<Classification> classes, ArrayList<Attribute> attributes) {
		double rootEntropy = calculateEntropy(classes);
		this.setWithInfoRatio(false);
		this.setWithPruning(false);
		setTree(new DecisionTree(classes, attributes, rootEntropy));
	}
	
	/**
	 * Calculates the information gain of each attribute of a node. 
	 * For each attribute, it calculates the information gain for all values and chooses the best value to 
	 * separate that attribute into two intervals (<= value and > value).
	 * @param node
	 */
	public void prepareAttributes(Node node) {
		
		//For each attribute, let's decide the interval that guarantees that max information gain possible
		for(Attribute attr : node.getAttributes()) {

			boolean needToDivideMore = true;
			
			if(!attr.isBelongsToTree()) {
				double infoGain = -1;
				double maxInfoGain = -1; //max information gain until now
				double partition = -1;
				double entropy1 = -1;
				double entropy2 = -1;
				ArrayList<Classification> classes1;//values <= partition
				ArrayList<Classification> classes2;//values > partition
				
				//try to divide the values by each value and calculate the entropy and information gain
				for(int i = 0; i < attr.getValues().size(); i++) {
					
					//the current value will be the partition
					partition = attr.getValues().get(i);
					
					//gets the two intervals by this partition (values <= partition and values > partition)
					classes1 = node.getClassificationsByPartition(partition, attr, false);
					classes2 = node.getClassificationsByPartition(partition, attr, true);
					
					if(classes1.size() > 0 && classes2.size() > 0) {//both intervals need to have elements
						//calculate entropy for the each interval
						entropy1 = calculateEntropy(classes1);
						entropy2 = calculateEntropy(classes2);				
						
						//calculate information gain
						infoGain = calcInfoGain(node.getEntropy(), node.getClasses().size(), 
								classes1.size(), classes2.size(), entropy1, entropy2);
						//if the information gain with this partition is better, update the attribute's related variables
						if(infoGain > maxInfoGain) {
							maxInfoGain = infoGain;
							attr.setPartition(partition);
							attr.setEntropy1(entropy1);
							attr.setEntropy2(entropy2);
							attr.setInformationGain(maxInfoGain);
						}
						needToDivideMore = false;
					}

				}

			}
		}

	}

	/**
	 * For a given node, chooses the attribute with the highest information gain.
	 * @param node
	 * @return the attribute with the highest information gain
	 */
	public Attribute chooseAttributeForNode(Node node) {
		Attribute chosenAttr = null;
		boolean start = true;
		
		for(Attribute attr : node.getAttributes()) {
			if(start) {
				chosenAttr = attr;
				start = false;
			}
			else {
				if(attr.getInformationGain() > chosenAttr.getInformationGain()) {
					chosenAttr = attr;
				}
			}
		}
		
		chosenAttr.setBelongsToTree(true);//this attribute is being used in the tree in this branch already
		node.setAttrName(chosenAttr.getName());//this node will be associated with this attribute's name
		return chosenAttr;
	}
	
	/**
	 * This works as the train function.
	 * For each node of the tree, and if that node isn't a leaf,
	 * prepares the attributes of that node in order to branch into two new nodes.
	 */
	public void buildTree() {
		long now = System.currentTimeMillis();//System.nanoTime();
		int i = 0;
		
		while(i < tree.getNodes().size()) {
			Node node = tree.getNodes().get(i);
			if(!node.isLeaf()) {
				prepareAttributes(node);
				Attribute atr = chooseAttributeForNode(node);	
				//build child nodes
				buildChildNodes(node, atr);
			}
			i++;
		}
		
		this.setTrainTime(System.currentTimeMillis() - now);//System.nanoTime() - now);
	}
	
	public boolean testing(boolean run) {
		
		tree.setGeneralRightClassifications(0);
		tree.setGeneralWrongClassifications(0);
		tree.setTotalClassifications(0);
		
		for(ClassifiedInstance instance : testData) {
			Node node = tree.getRoot();
			
			while(!node.isLeaf()) {
				String atrName = node.getAttrName();
				Double value = instance.getAttributeValues().get(atrName);
				Double partitionValue = node.getDividingValue();
				
				if(value <= partitionValue)
					node = node.getChildren().get("<=" + partitionValue);
				else
					node = node.getChildren().get(">" + partitionValue);
				
			}
			
			String actualClassification = node.getDecision().getName().toLowerCase();
			
			tree.increaseTotal();
			node.increaseTotal();
			
			if(instance.getClassification().getName().toLowerCase().equals(actualClassification)) {
				node.increaseRight();
				tree.increaseRight();
			}
			else {
				node.increaseWrong();
				tree.increaseWrong();
			}
				
		}
		
		//"Right: " + tree.getGeneralRightClassifications() + "\n Wrong: " + tree.getGeneralWrongClassifications();
		
		if(isWithPruning() && run)
			prune();
		
		return true;
	}
	
	/**
	 * Prunes the tree using the formula: (e+1)/(n+2)
	 * where e is the number of errors in a leaf and n is the number of classifications in the leaf
	 */
	public void prune() {
		
		for(int i = 0; i < 3; i++) {
			ArrayList<Node> newNodes = new ArrayList<Node>();
			
			for(Node n : tree.getNodes()) {
				//(e+1)/(n+2)
				
				//if it wasnt removed, add it back
				if(!n.getExcluded())
					newNodes.add(n);
				
				if(!n.isLeaf() && !n.isRoot()) {				
					
					Node child1 = null;
					Node child2 = null;
					boolean found = false;
					
					for(Entry<String, Node> entry : n.getChildren().entrySet()) {
						if(!found) {
							child1 = entry.getValue();
							found = true;
						}
						else
							child2 = entry.getValue();					
					}
					
					if(child1.isLeaf() && child2.isLeaf()) {
						double errorBothNodes = (child1.getErrorRate() + child2.getErrorRate())/2.0;
						n.setErrorMedia(errorBothNodes);
						//eliminate child nodes
						if(n.getErrorMedia() < CONFIDENCE) {
							n.setLeaf(true);
							n.setDecision(child1.getDecision());
							n.setChildren(new HashMap<String,Node>());
							child1.setExcluded(true);
							child2.setExcluded(true);
						}
						else if(child1.getDecision().getName().equals(child2.getDecision().getName())) {
							n.setLeaf(true);
							n.setDecision(child1.getDecision());
							n.setChildren(new HashMap<String,Node>());
							child1.setExcluded(true);
							child2.setExcluded(true);
						}
					}
				}
			}
			
			tree.setNodes(newNodes);
		}
		
		testing(false);
	}
	
	/**
	 * Creates two nodes and adds them to the tree and to the parentNode's list.
	 * Each node is initialized with its entropy. Depending on the entropy, the node might become a leaf and 
	 * won't branch further and a classification is calculated (end of the branch). 
	 * For both nodes, the new lists of attributes and classifications are passed on.
	 *  
	 * @param parentNode the node that originates the two new nodes
	 * @param atr attribute chosen
	 */
	public void buildChildNodes(Node parentNode, Attribute atr) {
		Node node1 = new Node(atr.getName(), false, parentNode);//<=
		Node node2 = new Node(atr.getName(), false, parentNode);//>
		
		node1.setEntropy(atr.getEntropy1());
		node2.setEntropy(atr.getEntropy2());
		
		//attributes and classifications
		ArrayList<Attribute> attrs1 = parentNode.getAttributesByPartition(atr.getPartition(), atr, false);
		ArrayList<Attribute> attrs2 = parentNode.getAttributesByPartition(atr.getPartition(), atr, true);

		node1.setAttributes(attrs1);
		node2.setAttributes(attrs2);
		
		ArrayList<Classification> classes1 = parentNode.getClassificationsByPartition(atr.getPartition(), atr, false);
		ArrayList<Classification> classes2 = parentNode.getClassificationsByPartition(atr.getPartition(), atr, true);
		node1.setClasses(classes1);
		node2.setClasses(classes2);
		
		//TODO
		if(node1.getEntropy() == 0 || attrs1.size() == 0)
			node1.setLeaf(true);
		else
			node1.setLeaf(false);
		
		if(node2.getEntropy() == 0 || attrs2.size() == 0)
			node2.setLeaf(true);
		else
			node2.setLeaf(false);
		
		//if the node is a leaf, set the decision
		if(node1.isLeaf()) {
			if(attrs1.size() == 0)
				node1.setDecision(new Classification(node1.getAttrName()));
			else
				node1.setDecision(node1.getClasses().get(0));
		}
		
		if(node2.isLeaf()) {
			if(attrs2.size() == 0)
				node2.setDecision(new Classification(node2.getAttrName()));
			else
				node2.setDecision(node2.getClasses().get(0));
		}

		parentNode.addChild(node1, "<="+ atr.getPartition());
		parentNode.addChild(node2, ">" + atr.getPartition());
		parentNode.setDividingValue(atr.getPartition());
		tree.addNode(node1);
		tree.addNode(node2);

	}
	
	/**
	 * Counts repeated occurrences of 'country' in 'classifications'
	 * @param country String to count occurrences of
	 * @param classifications ArrayList<Classification> where we will search for the String
	 * @return int - the number of repeated occurrences of 'country' in 'classifications'
	 */
	public int countRepeated(String country, ArrayList<Classification> classifications) {
		int count = 0;
		
		for(Classification c : classifications) {
			if(c.getName().equals(country))
				count++;
		}
		
		return count;
	}
	
	/**
	 * Calculates the entropy of our classes
	 * @param classifications - the classifications list to calculate the entropy for
	 * @return double - the entropy
	 */
	public double calculateEntropy(ArrayList<Classification> classifications) {
		double entropy = 0;
		ArrayList<String> seenCountries = new ArrayList<String>();
		ArrayList<Double> probabilities = new ArrayList<Double>();
		
		//for each different country we find in classifications, we will keep the probability of that country
		//(by dividing the number of occurrences of country by the total number of entries
		for(Classification c : classifications) {
			boolean found = false;
			for(String country : seenCountries) {
				if(country.equals(c.getName())) {
					found = true;
					break;
				}
			}
			
			if(!found) {
				double probability = countRepeated(c.getName(), classifications)/(double)classifications.size();
				probabilities.add(probability);
				seenCountries.add(c.getName());
			}
		}
		
		//int i = 0;
		for(Double probability : probabilities) {

			if(probability == 0)
				entropy += 0;
			else
				entropy += -probability * (Math.log(probability)/Math.log(2));

		}
		
		return entropy;
	}
	
	/**
	 * Calculates the information gain
	 * @param mainEntropy - the entropy of the node
	 * @param totalValues - total number of entries
	 * @param interval1TotalValues - number of entries in the first interval
	 * @param interval2TotalValues - number of entries in the second interval
	 * @param entropy1 - entropy of the first interval
	 * @param entropy2 - entropy of the second interval
	 * @return double - the information gain
	 */
	public double calcInfoGain(double mainEntropy, int totalValues, int interval1TotalValues, int interval2TotalValues, double entropy1, double entropy2) {
		double prob1 = (double)interval1TotalValues/totalValues;
		double prob2 = (double)interval2TotalValues/totalValues;
		double infoGain = mainEntropy - ((double)prob1*entropy1 + (double)prob2*entropy2);
		
		if(withInfoRatio) {
			double side1 = (prob1 == 0 ? 0 : prob1 * (Math.log(prob1)/Math.log(2)));
			double side2 = (prob2 == 0 ? 0 : prob2 * (Math.log(prob2)/Math.log(2)));		
			double infoSeparacao = - (side1 + side2);
			double infoRatio = infoGain / infoSeparacao;
			return infoRatio;
		}
		else {
			return infoGain;
		}

	}

	/**
	 * @return the tree
	 */
	public DecisionTree getTree() {
		return tree;
	}

	/**
	 * @param tree the tree to set
	 */
	public void setTree(DecisionTree tree) {
		this.tree = tree;
	}

	/**
	 * @return the withPruning
	 */
	public boolean isWithPruning() {
		return withPruning;
	}

	/**
	 * @param withPruning the withPruning to set
	 */
	public void setWithPruning(boolean withPruning) {
		this.withPruning = withPruning;
	}

	/**
	 * @return the withInfoRatio
	 */
	public boolean isWithInfoRatio() {
		return withInfoRatio;
	}

	/**
	 * @param withInfoRatio the withInfoRatio to set
	 */
	public void setWithInfoRatio(boolean withInfoRatio) {
		this.withInfoRatio = withInfoRatio;
	}

	/**
	 * @return the testData
	 */
	public ArrayList<ClassifiedInstance> getTestData() {
		return testData;
	}

	/**
	 * @param testData the testData to set
	 */
	public void setTestData(ArrayList<ClassifiedInstance> testData) {
		this.testData = testData;
	}
	
	public void updateOptions(boolean withPruning, boolean withRatio) {
		this.withPruning = withPruning;
		this.withInfoRatio = withRatio;
	}

	/**
	 * @return the errors
	 */
	public String getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(String errors) {
		this.errors = errors;
	}

	/**
	 * @return the trainTime
	 */
	public long getTrainTime() {
		return trainTime;
	}

	/**
	 * @param trainTime the trainTime to set
	 */
	public void setTrainTime(long trainTime) {
		this.trainTime = trainTime;
	}
	
	
}
