package logic.nonweka;

import java.util.ArrayList;

import utils.Parser;

public class Data {

	private ArrayList<Attribute> attrs;
	private ArrayList<Classification> classes;
	private ArrayList<ClassifiedInstance> testData;
	
	public Data() {
		attrs = new ArrayList<Attribute>();
		classes = new ArrayList<Classification>();
		testData = new ArrayList<ClassifiedInstance>();
	}
	
	public void setTrainData(String filename, boolean useTrainFileForTesting) {
		Parser p = new Parser(filename);
		ArrayList<Attribute> attrs1 = new ArrayList<Attribute>();
		ArrayList<Classification> classes1 = new ArrayList<Classification>();
		p.parseCitiesFile(attrs1, classes1);
		if(!useTrainFileForTesting) {
			this.attrs = attrs1;
			this.classes = classes1;
		}			
		else {
			
			
		}
	}
	
	public void setTestData(String filename) {
		Parser p = new Parser(filename);
		ArrayList<Attribute> attrs1 = new ArrayList<Attribute>();
		ArrayList<Classification> classes1 = new ArrayList<Classification>();
		p.parseCitiesFile(attrs1, classes1);
		testData = p.getTestData(attrs1, classes1);
	}
	
	/**
	 * @return the attrs
	 */
	public ArrayList<Attribute> getAttrs() {
		return attrs;
	}
	/**
	 * @param attrs the attrs to set
	 */
	public void setAttrs(ArrayList<Attribute> attrs) {
		this.attrs = attrs;
	}
	/**
	 * @return the classes
	 */
	public ArrayList<Classification> getClasses() {
		return classes;
	}
	/**
	 * @param classes the classes to set
	 */
	public void setClasses(ArrayList<Classification> classes) {
		this.classes = classes;
	}
	/**
	 * @return the testData
	 */
	public ArrayList<ClassifiedInstance> getTestData() {
		return testData;
	}
	/**
	 * @param testData the testData to set
	 */
	public void setTestData(ArrayList<ClassifiedInstance> testData) {
		this.testData = testData;
	}

	public void resetTrainData() {
		this.attrs.clear();
		this.classes.clear();
	}	

	public void resetTestData() {
		this.testData.clear();		
	}
}
