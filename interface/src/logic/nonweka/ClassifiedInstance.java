package logic.nonweka;

import java.util.Hashtable;

public class ClassifiedInstance {

	private Classification classification;
	private Hashtable<String,Double> attributeValues;
	
	public ClassifiedInstance(Classification c) {
		this.setClassification(c);
		this.setAttributeValues(new Hashtable<String,Double>());
	}
	
	/**
	 * @return the classification
	 */
	public Classification getClassification() {
		return classification;
	}

	/**
	 * @param classification the classification to set
	 */
	public void setClassification(Classification classification) {
		this.classification = classification;
	}

	/**
	 * @return the attributeValues
	 */
	public Hashtable<String,Double> getAttributeValues() {
		return attributeValues;
	}
	
	public void addAttributeValue(String attrName, Double value) {
		this.attributeValues.put(attrName, value);
	}

	/**
	 * @param attributeValues the attributeValues to set
	 */
	public void setAttributeValues(Hashtable<String,Double> attributeValues) {
		this.attributeValues = attributeValues;
	}
}
