package logic.nonweka;
import java.util.ArrayList;
import java.util.HashMap;

public class Node {

	private static int id = 0;				//to keep count of the ids
	private boolean isRoot;					//if this node is root or not
	private boolean isLeaf;					//if this node is a leaf (end of decision route) or not
	private Node parent;					//node's parent, null if isRoot == true
	private HashMap<String,Node> children; 	//node's children - used to navigate the tree. Each child node is 
											//connected to a String that represents the "decision" taken for this node
											//in the format "<= VALUE" or "> VALUE"
	private Double dividingValue;
	private String attrName;				//the attribute associated with this node, to display in a GraphViz image
	private int curId;						//the id of the node. this is used to draw the tree in dotty format (for GraphViz)
	private Classification decision; //if this node is a leaf node, it will have a classification
	
	
	//for training purposes
	private double entropy; //entropy of this node
	private ArrayList<Attribute> attributes;	//attributes used for the calculations
	private ArrayList<Classification> classes; 		//classifications
	
	//for testing purposes
	private int rightClassifications; //number of right classifications, if this is a leaf node
	private int wrongClassifications; //number of wrong classifications if this is a leaf node
	private int classifications;
	private double errorRate;
	private double errorMedia;
	private boolean excluded;
	
	/**
	 * Constructor.
	 * @param attribute
	 * @param isRoot
	 * @param parent
	 */
	public Node(String attribute, boolean isRoot, Node parent) {
		this.setAttrName(attribute);
		this.setRoot(isRoot);
		this.setChildren(new HashMap<String,Node>());
		this.setParent(parent);
		setAttributes(new ArrayList<Attribute>());
		setClasses(new ArrayList<Classification>());
		setCurId(id++);
		this.setWrongClassifications(0);
		this.setRightClassifications(0);
		this.setClassifications(0);
		this.setErrorMedia(0);
		this.setErrorRate(0);
		this.setExcluded(false);
	}
	
	/**
	 * 
	 * @param partition
	 * @param attr
	 * @param isBigger
	 * @return
	 */
	public ArrayList<Classification> getClassificationsByPartition(double partition, Attribute attr, boolean isBigger) {
		ArrayList<Classification> c = new ArrayList<Classification>();
		double value;
		
		for(int i = 0; i < attr.getValues().size(); i++) {
			value = attr.getValues().get(i);
			if(isBigger) {
				if(value > partition) {
					c.add(classes.get(i));
				}
			}
			else {
				if(value <= partition) {
					c.add(classes.get(i));
				}
			}
		}
		
		return c;
	}
	
	/**
	 * 
	 * @param partition
	 * @param attrToDivide
	 * @param isBigger
	 * @return
	 */
	public ArrayList<Attribute> getAttributesByPartition(double partition, Attribute attrToDivide, boolean isBigger) {
		ArrayList<Attribute> atrList = new ArrayList<Attribute>();
		
		for(Attribute atr : attributes) {
			if(!atr.isBelongsToTree()) {
				Attribute a = new Attribute(atr.getName());
				a.setValues(getAttributeValues(atr, attrToDivide, partition, isBigger));
				atrList.add(a);
			}
		}

		return atrList;
	}
	
	/**
	 * 
	 * @param atrToGetValuesFrom
	 * @param attributeForIndexes
	 * @param partition
	 * @param isBigger
	 * @return
	 */
	public ArrayList<Double> getAttributeValues(Attribute atrToGetValuesFrom, Attribute attributeForIndexes, double partition, boolean isBigger) {
		ArrayList<Double> values = new ArrayList<Double>();
		double value;
		
		for(int i = 0; i < attributeForIndexes.getValues().size(); i++) {
			value = attributeForIndexes.getValues().get(i);
			if(isBigger) {
				if(value > partition) {
					values.add(atrToGetValuesFrom.getValues().get(i));
				}
			}
			else {
				if(value <= partition) {
					values.add(atrToGetValuesFrom.getValues().get(i));
				}
			}
		}
		
		//System.out.println("NUMVALUES: " + values.size());
		return values;
	}
	
	/**
	 * @return the isRoot
	 */
	public boolean isRoot() {
		return isRoot;
	}
	/**
	 * @param isRoot the isRoot to set
	 */
	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}
	/**
	 * @return the isLeaf
	 */
	public boolean isLeaf() {
		return isLeaf;
	}
	/**
	 * @param isLeaf the isLeaf to set
	 */
	public void setLeaf(boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	/**
	 * @return the parent
	 */
	public Node getParent() {
		return parent;
	}
	/**
	 * @param parent the parent to set
	 */
	public void setParent(Node parent) {
		this.parent = parent;
	}
	/**
	 * @return the children
	 */
	public HashMap<String,Node> getChildren() {
		return children;
	}
	/**
	 * @param children the children to set
	 */
	public void setChildren(HashMap<String,Node> children) {
		this.children = children;
	}

	public void addChild(Node node, String name) {
		this.children.put(name, node);
	}
	/**
	 * @return the attrName
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * @param attrName the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 * @return the decision
	 */
	public Classification getDecision() {
		return decision;
	}

	/**
	 * @param decision the decision to set
	 */
	public void setDecision(Classification decision) {
		this.decision = decision;
	}
	
	
	/**
	 * @return the attributes
	 */
	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return the classes
	 */
	public ArrayList<Classification> getClasses() {
		return classes;
	}

	/**
	 * @param classes the classes to set
	 */
	public void setClasses(ArrayList<Classification> classes) {
		this.classes = classes;
	}

	/**
	 * @return the entropy
	 */
	public double getEntropy() {
		return entropy;
	}

	/**
	 * @param entropy the entropy to set
	 */
	public void setEntropy(double entropy) {
		this.entropy = entropy;
	}

	/**
	 * @return the curId
	 */
	public int getCurId() {
		return curId;
	}

	/**
	 * @param curId the curId to set
	 */
	public void setCurId(int curId) {
		this.curId = curId;
	}

	/**
	 * @return the dividingValue
	 */
	public Double getDividingValue() {
		return dividingValue;
	}

	/**
	 * @param dividingValue the dividingValue to set
	 */
	public void setDividingValue(Double dividingValue) {
		this.dividingValue = dividingValue;
	}

	/**
	 * @return the rightClassifications
	 */
	public int getRightClassifications() {
		return rightClassifications;
	}

	/**
	 * @param rightClassifications the rightClassifications to set
	 */
	public void setRightClassifications(int rightClassifications) {
		this.rightClassifications = rightClassifications;
	}

	/**
	 * @return the wrongClassifications
	 */
	public int getWrongClassifications() {
		return wrongClassifications;
	}

	/**
	 * @param wrongClassifications the wrongClassifications to set
	 */
	public void setWrongClassifications(int wrongClassifications) {
		this.wrongClassifications = wrongClassifications;
	}
	
	/**
	 * @return the classifications
	 */
	public int getClassifications() {
		return classifications;
	}

	/**
	 * @param classifications the classifications to set
	 */
	public void setClassifications(int classifications) {
		this.classifications = classifications;
	}

	public void increaseRight() {
		this.rightClassifications++;
	}
	
	public void increaseWrong() {
		this.wrongClassifications++;
	}

	public void increaseTotal() {
		this.setClassifications(this.getClassifications() + 1);
		
	}

	/**
	 * @return the errorRate
	 */
	public double getErrorRate() {
		return ((this.wrongClassifications + 1)/(double)(this.classifications + 2));
	}

	/**
	 * @param errorRate the errorRate to set
	 */
	public void setErrorRate(double errorRate) {
		this.errorRate = errorRate;
	}

	/**
	 * @return the errorMedia
	 */
	public double getErrorMedia() {
		return errorMedia;
	}

	/**
	 * @param errorMedia the errorMedia to set
	 */
	public void setErrorMedia(double errorMedia) {
		this.errorMedia = errorMedia;
	}

	public void setExcluded(boolean b) {
		this.excluded = b;
		
	}
	
	public boolean getExcluded() {
		return this.excluded;		
	}

}
