package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import logic.nonweka.DecisionTree;
import logic.nonweka.Node;
import weka.classifiers.trees.J48;

public class Utils {

    /**
     * Returns the extention of a file.
     * @param fileName
     * @return
     */
    public static String getExtension(String fileName) {
		String extension = "";

		int i = fileName.lastIndexOf('.');
		if (i > 0) {
		    extension = fileName.substring(i+1);
		}
		
		return extension;
    }
    
	/**
	 * Generates .dot file with the tree's graph in dotty format and respective tree image, using
	 * the GraphViz tools to generate images from .dot files.
     * @param dottyString a string in dotty format
     * @return true if success
     * @return false otherwise
     */
	public static boolean generateDottyFile(String dottyString, String filename) {
		PrintWriter writer = null;
		
		try {
			writer = new PrintWriter("bin/tree.dot", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}
		
		try {
			writer.println(dottyString);
		} catch (Exception e) {
			writer.close();
			return false;
		}
		
		writer.close();
		
		String input = "bin/tree.dot";
		   
		GraphViz gv = new GraphViz();
		gv.readSource(input);
		//System.out.println(gv.getDotSource());
		String type = "png";
		File out = new File("bin/"+filename+"."+type);
		gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), out );
		return true;
	}
	
	public static String getGraphVizString(DecisionTree tree) {
		StringBuilder strbldr = new StringBuilder();
		
		strbldr.append("digraph J48Tree {");
		
		int i = 0;
		for(Node node : tree.getNodes()) {
			if(!node.isLeaf()) {
				strbldr.append("N"+node.getCurId() + " [label=\"" + node.getAttrName() + "\"]");
			}
			else {
				strbldr.append("N"+node.getCurId() + " [label=\"" + node.getDecision().getName() + "\" shape=box style=filled]");
			}
			
			int j = 1;
			
			for (Entry<String, Node> entry : node.getChildren().entrySet()) {
			    String key = entry.getKey();
			    Node childNode = entry.getValue();
		    	strbldr.append("N"+node.getCurId() + "->" + "N"+childNode.getCurId() + " [label=\"" + key + "\"]");
		    	j++;
			}
			
			i++;
		}
		
		strbldr.append("}");
		return strbldr.toString();
	}
	
	/**
	 * Generates .dot file with the tree's graph in dotty format and respective tree image, using
	 * the GraphViz tools to generate images from .dot files. Uses getGraphVizString() function to return
	 * the tree in dotty format.
	 */
	public static boolean generateDottyFile(DecisionTree tree, String imageName) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("bin/"+imageName+"_tree_1.dot", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		try {
			writer.println(getGraphVizString(tree));
		} catch (Exception e) {
			return false;
		}
		writer.close();
		
		String input = "bin/"+imageName+"_tree_1.dot";
		   
		GraphViz gv = new GraphViz();
		gv.readSource(input);
		//System.out.println(gv.getDotSource());
		String type = "png";
		File out = new File("bin/"+imageName+"." + type);
		gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), out );
		return true;
	}
	
	/**
	 * Divides the value into time pieces
	 * @param value time
	 * @return
	 */
	public static long[] splitToComponentTimes(long value)
	{
		long[] time = new long[4];

		//hours
		time[0] = TimeUnit.MILLISECONDS.toHours(value);
		value -= TimeUnit.HOURS.toMillis(time[0]);

		//minutes
		time[1] = TimeUnit.MILLISECONDS.toMinutes(value);
		value -= TimeUnit.MINUTES.toMillis(time[1]);

		//seconds
		time[2] = TimeUnit.MILLISECONDS.toSeconds(value);
		value -= TimeUnit.SECONDS.toMillis(time[2]);
				
		time[3] = TimeUnit.MILLISECONDS.toMillis(value);
		
		return time;
	}
}
