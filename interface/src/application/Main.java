package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		
		
       Parent root = null;
       
		try {
			root = FXMLLoader.load(getClass().getResource("Interface.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
       
        Scene scene = new Scene(root);
    
        primaryStage.setTitle("Origem Geografica De Musica");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
