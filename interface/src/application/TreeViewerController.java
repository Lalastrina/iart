package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class TreeViewerController implements Initializable  {
    
	@FXML
	private AnchorPane treeViewPane;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
        Image img = new Image(InterfaceController.getImageName()+".png");
        if(img != null) {
        	//Image img = new Image("test.jpg");
            ImageView imgView = new ImageView(img);
            final ScrollPane sp = new ScrollPane();
            sp.setMinHeight(treeViewPane.getMinHeight());
            sp.setMinWidth(treeViewPane.getMinWidth());
            sp.setContent(imgView);
            sp.setHbarPolicy(ScrollBarPolicy.ALWAYS);
            sp.setVbarPolicy(ScrollBarPolicy.ALWAYS);
            treeViewPane.getChildren().add(sp);
            
            treeViewPane.heightProperty().addListener(new ChangeListener<Number>() {

    			@Override
    			public void changed(ObservableValue<? extends Number> arg0,
    					Number arg1, Number arg2) {
    				sp.setMinHeight(Double.parseDouble(arg2.toString()));				
    			}
            	
            });
            
            treeViewPane.widthProperty().addListener(new ChangeListener<Number>() {

    			@Override
    			public void changed(ObservableValue<? extends Number> arg0,
    					Number arg1, Number arg2) {
    				sp.setMinWidth(Double.parseDouble(arg2.toString()));				
    			}
            	
            });

        }
	}

}
