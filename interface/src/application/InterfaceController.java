package application;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import utils.Utils;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import logic.nonweka.C45;
import logic.nonweka.Data;
import logic.weka.WekaData;
import logic.weka.WekaTrain;

public class InterfaceController implements Initializable {
	
	private static final String FAIL_COLOR = "Red";
	private static final String WORKING_COLOR = "Green";
	private static final String NEUTRAL_COLOR = "Black";
	
	//Structures
	WekaData wekaData = new WekaData();
	WekaTrain wekaTrain = new WekaTrain(false);
	Data data = new Data();
	C45 c45;
	
	//Interface
    final ScrollPane sp = new ScrollPane();
    private boolean treeWindowOpen = false;
	private static String treeImageName;
	private String filename = "";
	
	// OPTIONS
	private boolean useTrainFileForTesting = false;	
	//own implementation
	private boolean usePruning = false;
	private boolean useRatio = false;
	//weka
	private boolean wekaUsePruning = false;
	private boolean useCrossValidation = false;
	private int numFolds = 1;
	private String confidence = "0.25";
	
	// FILE CHOOSING SECTION
    @FXML
    private TextField trainingFileTextField;    
    @FXML
    private TextField testingFileTextField;	
    @FXML
    private Label fileChooserLabel;
    @FXML
    private CheckBox useTrainingFileCheckbox;
    
    // WEKA
    @FXML
    private Label foldsLabel;
    @FXML
    private Slider foldsSlider;
    @FXML
    private CheckBox useCrossValidationCheckbox;
    @FXML
    private ToggleGroup wekaPruningGroup;
    
    // NOT WEKA 
    @FXML
    private ToggleGroup pruningGroup;
    @FXML
    private CheckBox useRatioCheckbox;
    
    // RUN
    @FXML
    private Label trainStatusLabel;
    @FXML
    private AnchorPane resultsScrollPane;
    
    private boolean isWeka;
    
    // EVENTS
    
    @FXML
    void chooseTestFileHandler(ActionEvent event) {
    	chooseFileHandler(false);
    }
    
    @FXML
    void chooseTrainFileHandler(ActionEvent event) {
    	chooseFileHandler(true);    	
    }
    
    @FXML
    void wekaTrainHandler(ActionEvent event) {
    	isWeka = true;
    	if(wekaData.getTrainData() != null) {
    		
    		if(useCrossValidation || useTrainFileForTesting) {
    			wekaCrossValidation();
    		}
    		else {
    			wekaTrainAndTest();
    		}
    		
    	}
    	else {
    		trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
    		trainStatusLabel.setText("No training data loaded.");
    	}
    }
    
    @FXML
    void trainHandler(ActionEvent event) {
    	isWeka = false;
    	
    	if(data.getClasses().size() != 0 && data.getAttrs().size() != 0) {
    		c45 = new C45(data.getClasses(), data.getAttrs());
    		c45.setTestData(data.getTestData());
    		trainAndTest();
    	}
    	else {
    		trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
    		trainStatusLabel.setText("No training data loaded.");
    	}

    }
    
    // AUX FUNCTIONS FOR FILE CHOOSING SECTION
    
    /**
     * Weka cross validation
     */
    private void wekaCrossValidation() {
    	
		if(data != null && data.getAttrs() != null && data.getAttrs().size() > 0 && data.getAttrs().get(0).getValues().size() > 0 && numFolds > data.getAttrs().size()) {
			numFolds = data.getAttrs().get(0).getValues().size();					
		}
		
    	wekaTrain.updateOptions(wekaUsePruning, numFolds, confidence);
    	
    	//cross validate
    	if(!wekaTrain.crossValidation(wekaData.getTrainData())) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText(wekaTrain.getErrors());
			wekaTrain.setErrors("");
			return;
    	}
    	
		String pruning;
		
		if(wekaUsePruning) {
			pruning="pruning_"+confidence+"confidence";
		}
		else
			pruning="nopruning";
			
		treeImageName = filename+"_tree_weka_"+pruning+"_"+numFolds+"folds";
		wekaShowTreeAndStats(treeImageName);
    	
    }
    
    /**
     * Weka training + testing + tree viewing
     */
    private void wekaTrainAndTest() {
    	
		wekaTrain.updateOptions(wekaUsePruning, numFolds, confidence);
		
		if(wekaData.getTestData() == null) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("No testing data loaded.");
			return;
		}
		
		if(!wekaTrain.train(wekaData.getTrainData())) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("Error training:\n"+wekaTrain.getErrors());
			wekaTrain.setErrors("");
			return;
		}
		
		if(!wekaTrain.test(wekaData.getTestData(), wekaData.getTrainData())) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("Error testing:\n"+wekaTrain.getErrors());
			wekaTrain.setErrors("");
			return;
		}
		
		String pruning;
		
		if(wekaUsePruning)
			pruning = "pruning_"+confidence+"confidence";
		else
			pruning="nopruning";
			
		treeImageName = filename+"_tree_weka_"+pruning;
		wekaShowTreeAndStats(treeImageName);

    }
    
    private void trainAndTest() {
    	c45.updateOptions(usePruning, useRatio);
    	
		if(data.getTestData().size() == 0) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("No testing data loaded.");
			return;
		}
		
		c45.buildTree();
		
		if(!c45.testing(true)) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("Error testing:\n"+c45.getErrors());
			c45.setErrors("");
			return;
		}
		
		String pruning;
		String ratio;
		
		if(usePruning)
			pruning = "pruning";
		else
			pruning ="nopruning";
		
		if(useRatio)
			ratio = "withratio";
		else
			ratio = "noratio";		
			
		treeImageName = filename+"_tree_nonweka_"+pruning+"_"+ratio;
		showTreeAndStats(treeImageName);    	
    }
    
   private void showTreeAndStats(String filename) {
		if(!Utils.generateDottyFile(c45.getTree(), filename)) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("Error generating tree. Perhaps the training failed?");
		}
		else {
			trainStatusLabel.setTextFill(Paint.valueOf(WORKING_COLOR));
			trainStatusLabel.setText("Sucessfully Trained.\n");
			Label stats = new Label();
			stats.setText(showStats());
			sp.setContent(stats);
			showSecondStage();
		}		
	}
    
   private void wekaShowTreeAndStats(String filename) {
		if(!Utils.generateDottyFile(wekaTrain.getTreeInDottyFormat(), filename)) {
			trainStatusLabel.setTextFill(Paint.valueOf(FAIL_COLOR));
			trainStatusLabel.setText("Error generating tree. Perhaps the training failed?");
		}
		else {
			trainStatusLabel.setTextFill(Paint.valueOf(WORKING_COLOR));
			trainStatusLabel.setText("Sucessfully Trained.\n");
			Label stats = new Label();
			stats.setText(showStats());
			sp.setContent(stats);
			showSecondStage();
		}		
	}

   String showStats() {    
    	
        if(isWeka && wekaTrain.getEvaluation() == null) {
        	return "Nothing to show. No evaluation has been done yet.";      
        }
        
        DecimalFormat f = new DecimalFormat("##.00");
        StringBuilder builder = new StringBuilder();
        //builder.append(wekaTrain.getEvaluation().toSummaryString());
        
        if(isWeka) {
        	builder.append("Total number of instances: " + wekaTrain.getEvaluation().numInstances() + "\n");
        }
        else {
        	builder.append("Total number of instances: " + c45.getTree().getTotalClassifications() + "\n");
        } 
        
        builder.append("Correctly Classified Instances: ");
        
        if(isWeka) {
        	builder.append(wekaTrain.getEvaluation().correct() + " (");
        	builder.append(f.format(wekaTrain.getEvaluation().pctCorrect()) + " %)\n");
        }
        else {
        	builder.append(c45.getTree().getGeneralRightClassifications() + " (");
        	builder.append(f.format(c45.getTree().getRightPct()) + " %)\n");
        }        
        
        builder.append("Incorrectly Classified Instances: ");
        
        if(isWeka) {
            builder.append(wekaTrain.getEvaluation().incorrect() + " (");
            builder.append(f.format(wekaTrain.getEvaluation().pctIncorrect()) + " %)\n"); 
        }
        else {
        	builder.append(c45.getTree().getGeneralWrongClassifications() + " (");
        	builder.append(f.format(c45.getTree().getWrongPct()) + " %)\n");
        }       

        if(isWeka) {
	        builder.append("Unclassified Instances: ");
	        builder.append(wekaTrain.getEvaluation().unclassified() + " (");
	        builder.append(f.format(wekaTrain.getEvaluation().pctUnclassified()) + " %)\n"); 
        }
        
        builder.append("Total number of nodes: ");
        
        if(isWeka) {
        	builder.append(wekaTrain.getNumberOfNodes() + "\n"); 
        }
        else {
        	builder.append(c45.getTree().getNodes().size() + "\n"); 
        }        
        
        builder.append("Number of leaves: ");
        
        if(isWeka) {
        	 builder.append(wekaTrain.getNumberOfLeaves() + "\n"); 
        }
        else {
        	builder.append(c45.getTree().getNumLeaves() + "\n");        	
        }
        
        builder.append("Time taken to train: ");
        
        long time[];
        
        if(isWeka) {
        	time = Utils.splitToComponentTimes(wekaTrain.getTrainTime()); 
        }
        else {
        	time = Utils.splitToComponentTimes(c45.getTrainTime());         	
        }        
        
        builder.append(time[0] + " hours " + time[1] + " minutes " + time[2] + " seconds " + time[3] + " miliseconds.\n");
        
        return builder.toString();
    }
   
    /**
     * 
     * @param file
     * @param isTraining
     * @param type
     * @return
     */
    private String validateFile(File file, boolean isTraining, String type) {
    	String errors = "";
    	String filepath = "";
    	boolean isError = false;
    	
    	if (file != null) {
    		if(file.exists()) {
        		filepath = file.getAbsolutePath();
        		String filename = file.getName();
        		String extension = Utils.getExtension(filename).toLowerCase();
        		if(extension.equals("arff")) {
        			
        			if(isTraining) {
        				wekaData.setTrainData(filepath);
        				data.setTrainData(filepath, useTrainFileForTesting);
        			}
        			else {
        				wekaData.setTestData(filepath);
        				data.setTestData(filepath);
        			}
        			
        			if(wekaData.getErrors().length() != 0) {
            			errors = wekaData.getErrors();
            			isError = true;   
            			wekaData.cleanErrors();
        			}
        			else {
        				this.filename = file.getName();
            			errors = type+" file has the correct format and structure.";
            			isError = false;
        			}
        		}
        		else {
        			errors = type+" file has the wrong file format.";
        			isError = true;
        		}
    		}
    		else {
    			errors = type+" file doesn't exist.";
    			isError = true;
    		}
    	}
    	else {
    		errors = "No "+type+" file selected.";
    		isError = true;
    	}
    	
    	handleFileChange(errors, filepath, isError, isTraining);
    	
    	return errors;
    }
    
    private void chooseFileHandler(boolean isTraining) {

    	String type = (isTraining? "Training" : "Testing");
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.setTitle("Choose "+type+" File");
    	File file = fileChooser.showOpenDialog(null);
    	
    	validateFile(file,isTraining,type);
    }
    
	private void handleFileChange(String message, String filename, boolean isError, boolean isTraining) {
		if(isError) {
			fileChooserLabel.setTextFill(Paint.valueOf(FAIL_COLOR));			
			if(isTraining) {
				trainingFileTextField.setText("");
				wekaData.resetTrainData();
				data.resetTrainData();
			}
			else  {
				testingFileTextField.setText("");
				wekaData.resetTestData();
				data.resetTestData();
			}
		}
		else {
			fileChooserLabel.setTextFill(Paint.valueOf(WORKING_COLOR));
			if(isTraining) {
				trainingFileTextField.setText(filename);
				//wekaData.setTrainData(filename);
			}
			else {
				testingFileTextField.setText(filename);
				//wekaData.setTestData(filename);
			}
		}
		
		fileChooserLabel.setText(message);
			
	}

    boolean showSecondStage() {
    	if(!treeWindowOpen) {

	    	Stage secondaryStage = new Stage();
	    	Parent root = null;
	        
			try {
				root = FXMLLoader.load(getClass().getResource("TreeViewer.fxml"));
			} catch (IOException e) {
				e.printStackTrace();
			}
	       
	        Scene scene = new Scene(root);
	        
	        secondaryStage.setTitle("Viewing Tree");
	        secondaryStage.setResizable(true);
	        secondaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	            public void handle(WindowEvent we) {
	                treeWindowOpen = false;
	            }
	        });
	        treeWindowOpen = true;
	        secondaryStage.setScene(scene);
	        secondaryStage.show();
	        return true;
    	}
    	else
    		return false;
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		trainingFileTextField.setText("");
		testingFileTextField.setText("");
		
		//sets the width and height of the results scroll pane
        sp.setMinHeight(resultsScrollPane.getMinHeight());
        sp.setMinWidth(resultsScrollPane.getMinWidth());
        sp.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        sp.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        resultsScrollPane.getChildren().add(sp);        
        
		/*trainingFileTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				if(!oldValue.equals(newValue)){
					File file = new File(newValue);
					System.out.println(newValue);
					//validateFile(file, true, "Training");
				}
				
			}
					
		});	
		
		testingFileTextField.textProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				if(!oldValue.equals(newValue)){
					File file = new File(newValue);
					testingFileTextField.setText(newValue);
					validateFile(file, false, "Testing");
				}
				
			}
					
		});	*/
		
		pruningGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			@Override
			public void changed(ObservableValue<? extends Toggle> observable,
					Toggle oldValue, Toggle newValue) {
				
				String id = ((RadioButton)pruningGroup.getSelectedToggle()).getId();
				
				if(id.equals("nopruning"))
					usePruning = false;
				else
					usePruning = true;
			}					
		});	
		
		wekaPruningGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			@Override
			public void changed(ObservableValue<? extends Toggle> observable,
					Toggle oldValue, Toggle newValue) {
				
				String id = ((RadioButton)wekaPruningGroup.getSelectedToggle()).getId();
				
				if(id.equals("nopruning"))
					wekaUsePruning = false;
				else if(id.equals("pruning025")) {
					wekaUsePruning = true;
					confidence = "0.25";
				}
			}					
		});	
		
		
		useRatioCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
					
					useRatio = newValue;
				
			}
				
		});	
		
		useCrossValidationCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
					
					useCrossValidation = newValue;
				
			}
				
		});
		
		/*useTrainingFileCheckbox.selectedProperty().addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable,
					Boolean oldValue, Boolean newValue) {
					
					useTrainFileForTesting = newValue;
				
			}
				
		});	*/
		
		//adds a listener to the slider to change fold number
		foldsSlider.valueProperty().addListener(new ChangeListener<Number>() {

			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number arg1, Number arg2) {
				
				//sets the number of folds
				numFolds = (int)Double.parseDouble(arg2.toString());
				
				if(data != null && data.getAttrs() != null && data.getAttrs().size() > 0 && data.getAttrs().get(0).getValues().size() > 0 && numFolds > data.getAttrs().size()) {
					numFolds = data.getAttrs().get(0).getValues().size();					
				}
				
				foldsLabel.setText(Integer.toString(numFolds)); //show value changed
				
			}
			
		});
	}

	public static String getImageName() {
		return treeImageName;
	}

}
