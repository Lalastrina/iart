package main;
import java.util.ArrayList;

public class MusicOrigin {
	
	public static void main(String[] args)
	{
		String fileName = "cidades_teste.txt";
		String fileName2 = "cidades.txt";
		String file = "bin/" + fileName2;
		
		Parser p = new Parser(file);
		ArrayList<Attribute> attrs = new ArrayList<Attribute>();
		ArrayList<Classification> classes = new ArrayList<Classification>();	
		p.parseCitiesFile(attrs, classes);		
		
		C45_2 c45_2 = new C45_2(classes, attrs);
		c45_2.setTestData(p.getTestData(attrs, classes));
		
		//train
		c45_2.buildTree();
		
		//test
		System.out.println(c45_2.testing());
		
		/*//p.treatAttributes(attrs, classes);
		C45 c45 = new C45();
		c45.setClasses(classes);
		c45.setAttributes(attrs);
		
		c45.doMagic();*/
		
		Interface i = new Interface();
		//System.out.println(i.getGraphVizString(c45_2.getTree()));
		//System.out.println("\n");
		i.generateDottyFile(c45_2.getTree(),fileName2);
	}

}
