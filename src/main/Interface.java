package main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map.Entry;

import utils.GraphViz;

public class Interface {

	public String getGraphVizString(DecisionTree tree) {
		StringBuilder strbldr = new StringBuilder();
		
		strbldr.append("digraph J48Tree {");
		
		int i = 0;
		for(Node node : tree.getNodes()) {
			if(!node.isLeaf()) {
				strbldr.append("N"+node.getCurId() + " [label=\"" + node.getAttrName() + "\"]");
			}
			else {
				strbldr.append("N"+node.getCurId() + " [label=\"" + node.getDecision().getName() + "\" shape=box style=filled]");
			}
			
			int j = 1;
			
			for (Entry<String, Node> entry : node.getChildren().entrySet()) {
			    String key = entry.getKey();
			    Node childNode = entry.getValue();
		    	strbldr.append("N"+node.getCurId() + "->" + "N"+childNode.getCurId() + " [label=\"" + key + "\"]");
		    	j++;
		    	String hue = childNode.isLeaf()? childNode.getDecision().getName() : childNode.getAttrName();
				System.out.println(node.getAttrName() + " with " + key + " to " + hue);
			}
			
			i++;
		}
		
		strbldr.append("}");
		return strbldr.toString();
	}
	
	/**
	 * Generates .dot file with the tree's graph in dotty format and respective tree image, using
	 * the GraphViz tools to generate images from .dot files. Uses getGraphVizString() function to return
	 * the tree in dotty format.
	 */
	public boolean generateDottyFile(DecisionTree tree, String imageName) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("bin/"+imageName+"_tree_1.dot", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		try {
			writer.println(getGraphVizString(tree));
		} catch (Exception e) {
			return false;
		}
		writer.close();
		
		String input = "bin/"+imageName+"_tree_1.dot";
		   
		GraphViz gv = new GraphViz();
		gv.readSource(input);
		//System.out.println(gv.getDotSource());
		String type = "png";
		File out = new File("bin/"+imageName+"_tree." + type);
		gv.writeGraphToFile( gv.getGraph( gv.getDotSource(), type ), out );
		return true;
	}

}
