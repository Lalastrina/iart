package main;
import java.util.ArrayList;

public class DecisionTree {

	private ArrayList<Node> nodes;
	private Node root;
	
	private int generalWrongClassifications;
	private int generalRightClassifications;
	
	/**
	 * Constructor.
	 * @param classes
	 * @param attributes
	 * @param rootEntropy
	 */
	public DecisionTree(ArrayList<Classification> classes, ArrayList<Attribute> attributes, double rootEntropy) {
		setNodes(new ArrayList<Node>());
		setRoot(classes, attributes, rootEntropy);
		nodes.add(root);
		setGeneralRightClassifications(0);
		setGeneralWrongClassifications(0);
	}

	/**
	 * Default constructor.
	 */
	public DecisionTree() {
		setNodes(new ArrayList<Node>());
		setRoot(null);
	}
	
	/**
	 * Adds a node to this decision tree
	 * @param node
	 */
	public void addNode(Node node) {
		this.nodes.add(node);
	}

	/**
	 * @return the nodes
	 */
	public ArrayList<Node> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes the nodes to set
	 */
	public void setNodes(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}

	/**
	 * @return the root
	 */
	public Node getRoot() {
		return root;
	}

	/**
	 * @param root the root to set
	 */
	public void setRoot(Node root) {
		this.root = root;
	}
	
	/**
	 * Creates the root with parameters passed.
	 * @param classes
	 * @param attributes
	 * @param rootEntropy
	 */
	public void setRoot(ArrayList<Classification> classes, ArrayList<Attribute> attributes, double rootEntropy) {
		this.root = new Node("", true, null);
		this.root.setAttributes(attributes);
		this.root.setClasses(classes);
		this.root.setEntropy(rootEntropy);
	}

	/**
	 * @return the generalWrongClassifications
	 */
	public int getGeneralWrongClassifications() {
		return generalWrongClassifications;
	}

	/**
	 * @param generalWrongClassifications the generalWrongClassifications to set
	 */
	public void setGeneralWrongClassifications(int generalWrongClassifications) {
		this.generalWrongClassifications = generalWrongClassifications;
	}

	/**
	 * @return the generalRightClassifications
	 */
	public int getGeneralRightClassifications() {
		return generalRightClassifications;
	}

	/**
	 * @param generalRightClassifications the generalRightClassifications to set
	 */
	public void setGeneralRightClassifications(int generalRightClassifications) {
		this.generalRightClassifications = generalRightClassifications;
	}
	
	public void increaseRight() {
		this.generalRightClassifications++;
	}
	
	public void increaseWrong() {
		this.generalWrongClassifications++;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TREES MAN";
	}

}
