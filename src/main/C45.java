package main;
import java.util.ArrayList;
import java.util.Random;

/**
 * Contains methods related to the C4.5 algorithm, used to 
 * get a decision tree.
 * 
 */
public class C45 {
	
	private ArrayList<Attribute> attributes;	//attributes used for the calculations
	private ArrayList<Classification> classes; 		//classification
	private DecisionTree tree; 					//generated tree
	
	/**
	 * Constructor.
	 */
	public C45() {
		setAttributes(new ArrayList<Attribute>());
		setClasses(new ArrayList<Classification>());
		setTree(new DecisionTree());
	}
	
	/**
	 * Counts repeated occurrences of 'country' in 'classifications'
	 * @param country String
	 * @param classifications ArrayList<Classification>
	 * @return int - the number of repeated occurrences of 'country' in 'classifications'
	 */
	public int countRepeated(String country, ArrayList<Classification> classifications) {
		int count = 0;
		
		for(Classification c : classifications) {
			if(c.getName().equals(country))
				count++;
		}
		
		return count;
	}
	
	/**
	 * 
	 * @param attr
	 * @param value
	 * @param isBigger
	 * @return
	 */
	public ArrayList<Classification> getClassesForAttribute(Attribute attr, double value, boolean isBigger) {
		ArrayList<Classification> c = new ArrayList<Classification>();
		
		if(isBigger)
			for(Integer i : attr.getIndexesAbove(value))
				c.add(classes.get(i));
		else
			for(Integer i : attr.getIndexesBelowOrEqual(value))
				c.add(classes.get(i));
		
		return c;
	}
	
	/**
	 * 
	 * @param attr
	 * @param value
	 * @param isBigger
	 * @return
	 */
	public ArrayList<Attribute> getAttributesForAttribute(Attribute attr, double value, boolean isBigger) {
		ArrayList<Attribute> c = new ArrayList<Attribute>();
		
		if(isBigger)
			for(Integer i : attr.getIndexesAbove(value))
				c.add(attributes.get(i));
		else
			for(Integer i : attr.getIndexesBelowOrEqual(value))
				c.add(attributes.get(i));
		
		return c;
	}
	
	/**
	 * 
	 */
	public void prepareAttributes() {
		
		for(Attribute t : attributes) {
		
			t.chooseInterval();
		}
	}
	
	/**
	 * Calculates the entropy of our classes
	 * @param classifications - the classifications list to calculate the entropy for
	 * @return double - the entropy
	 */
	public double calculateEntropy(ArrayList<Classification> classifications) {
		double entropy = 0;
		ArrayList<String> seenCountries = new ArrayList<String>();
		ArrayList<Double> probabilities = new ArrayList<Double>();
		
		for(Classification c : classifications) {
			boolean found = false;
			for(String country : seenCountries) {
				if(country.equals(c.getName())) {
					found = true;
					break;
				}
			}
			
			if(!found) {
				double probability = countRepeated(c.getName(), classifications)/(double)classifications.size();
				probabilities.add(probability);
				seenCountries.add(c.getName());
			}
		}
		
		//int i = 0;
		for(Double probability : probabilities) {
			
			//System.out.println("Probability of " + seenCountries.get(i) + ": " + probability);
			entropy += -probability * (Math.log(probability)/Math.log(2));
			//System.out.println("Entropy: " + entropy);
			//System.out.println("\n");
			//i++;
		}
		
		return entropy;
	}
	
	/**
	 * Calculates the entropy of our classes
	 * @return double - the entropy
	 */
	public double calculateEntropy() {
		double entropy = 0;
		ArrayList<String> seenCountries = new ArrayList<String>();
		ArrayList<Double> probabilities = new ArrayList<Double>();
		
		for(Classification c : classes) {
			boolean found = false;
			for(String country : seenCountries) {
				if(country.equals(c.getName())) {
					found = true;
					break;
				}
			}
			
			if(!found) {
				double probability = countRepeated(c.getName(), classes)/(double)classes.size();
				probabilities.add(probability);
				seenCountries.add(c.getName());
			}
		}
		
		//int i = 0;
		for(Double probability : probabilities) {
			
			//System.out.println("Probability of " + seenCountries.get(i) + ": " + probability);
			entropy += -probability * (Math.log(probability)/Math.log(2));
			//System.out.println("Entropy: " + entropy);
			//System.out.println("\n");
			//i++;
		}
		
		return entropy;
	}
	
	public void doMagic() {
		
		boolean isRoot = true;
		//Node currentNode = null;
		double mainEntropy = calculateEntropy();
		double maxInfoGain = -1;
		Attribute chosenAttribute = null;
		double infoGain;
		
		prepareAttributes();
		
		for(Attribute attr : attributes) {
			infoGain = calcInfoGain(mainEntropy, attr);
			attr.setInformationGain(infoGain);
			if(infoGain > maxInfoGain) {
				maxInfoGain = infoGain;
				chosenAttribute = attr;
			}
		}

		
		Node root = new Node(chosenAttribute.getName(), true, null);
		Node node1 = new Node(chosenAttribute.getName(), false, root);
		Node node2 = new Node(chosenAttribute.getName(), false, root);
		node1.setLeaf(true);
		node2.setLeaf(true);
		root.addChild(node1, "<=" + chosenAttribute.getPartition());
		root.addChild(node2, ">" + chosenAttribute.getPartition());
		tree.setRoot(root);
		tree.addNode(root);
		tree.addNode(node1);
		tree.addNode(node2);
		//currentNode = node;
		isRoot = false;

	}
	
	public double calcInfoGain(double mainEntropy, Attribute attr) {
		ArrayList<Classification> interval1 = new ArrayList<Classification>();
		ArrayList<Classification> interval2 = new ArrayList<Classification>();
		interval1 = getClassesForAttribute(attr, attr.getPartition(), true);
		interval2 = getClassesForAttribute(attr, attr.getPartition(), false);
			
		double entropy1 = calculateEntropy(interval1);
		double entropy2 = calculateEntropy(interval2);
		double prob1 = (double)interval1.size()/attr.getValues().size();
		double prob2 = (double)interval2.size()/attr.getValues().size();
		double infoGain = mainEntropy - (prob1*entropy1 + prob2*entropy2);
		return infoGain;
	}
	
	public double[] getProbabilities(int totalValues, Attribute attr, int partition) {
		double probability[] = new double[2];
		probability[0] = attr.countValuesBelowOrEqual(partition)/totalValues;
		probability[1] = attr.countValuesAbove(partition)/totalValues;
		return probability;
	}
	
	/**
	 * Adds an attribute to this decision trees attributes
	 * @param attr
	 */
	public void addAttribute(Attribute attr) {
		this.attributes.add(attr);
	}
	
	/**
	 * @return the attributes
	 */
	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}


	/**
	 * @return the tree
	 */
	public DecisionTree getTree() {
		return tree;
	}


	/**
	 * @param tree the tree to set
	 */
	public void setTree(DecisionTree tree) {
		this.tree = tree;
	}


	/**
	 * @return the classes
	 */
	public ArrayList<Classification> getClasses() {
		return classes;
	}


	/**
	 * @param classes the classes to set
	 */
	public void setClasses(ArrayList<Classification> classes) {
		this.classes = classes;
	}
}
