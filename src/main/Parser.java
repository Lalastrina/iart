package main;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Parser {

	private String filename;
	private boolean useWholeFileForTraining;
	
	public Parser(String filename) {
		
		this.filename = filename;	
		this.setUseWholeFileForTraining(true);
	}
	
	public ArrayList<ClassifiedInstance> getTestData(ArrayList<Attribute> attrs, ArrayList<Classification> classes) {
		ArrayList<ClassifiedInstance> testData = new ArrayList<ClassifiedInstance>();
		
		for(int i = 0; i < classes.size(); i++) {
			ClassifiedInstance instance = new ClassifiedInstance(classes.get(i));
			
			for(Attribute attr : attrs) {
				instance.addAttributeValue(attr.getName(), attr.getValues().get(i));
			}
			
			testData.add(instance);
		}
		
		return testData;
	}
	
	public void parseCitiesFile(ArrayList<Attribute> attrs, ArrayList<Classification> classes) {
		
		BufferedReader reader = null;
		
		try
		{
			File data_file = new File(filename);
			reader = new BufferedReader(new FileReader(data_file));
			
			String line;
			
			//Saves the lines(cases) of the file
			ArrayList<String> temp = new ArrayList<String>();
			while ((line = reader.readLine()) != null)
			{
				temp.add(line);
			}
			
			String[] array;
			
			//parses the file
			for(int i = 0; i < temp.size(); i++)
			{
				array = temp.get(i).split(",");
				for(int j = 0; j < array.length; j++) {
					//matrix[i][j] = Double.parseDouble(array[j]);
					if(j == (array.length - 1))//last column is the country name
						classes.add(new Classification(array[j]));			
					else {
						if(i == 0)
							attrs.add(new Attribute("a"+j));	
						attrs.get(j).addValue(array[j]);
					}
				}
				
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		} finally {
			try	{
				reader.close();
			} catch(IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	public void treatCoordinates(ArrayList<Attribute> attrs) {
		ArrayList<String> coordinates = new ArrayList<String>();
		boolean found;
		
		for(int i = 0; i < attrs.get(68).getValues().size(); i++) {
			found = false;
			String soNice = attrs.get(68).getValues().get(i) + "," + attrs.get(69).getValues().get(i);
			for(String e : coordinates) {
				if(e.equals(soNice)) {
					found = true;
					break;
				}
			}
			if(!found)
				coordinates.add(soNice);			
		}
		
		for(String e : coordinates)
			System.out.println(coordinates.indexOf(e) + " - " + e);
	}
	
	public void treatAttributes(ArrayList<Attribute> attrs, ArrayList<Classification> classes) {
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder stringBuilderCountry = new StringBuilder();
		ArrayList<String> countries = new ArrayList<String>();
		
		//Build the string for the countries
		for(Classification c : classes) {
			if(!countries.contains(c.getName()))
				countries.add(c.getName());
		}
		
		stringBuilderCountry.append("@attribute classes {");
		for(int i = 0; i < countries.size(); i++) {
			stringBuilderCountry.append(countries.get(i));
			if(!(i == countries.size() - 1)) stringBuilderCountry.append(", ");
		}
		stringBuilderCountry.append("}");
		
		//build all the strings for the different attributes
		for(Attribute c : attrs) {
			stringBuilder.append("@attribute ");
			stringBuilder.append(c.getName());
			stringBuilder.append(" numeric\n");
		}		

		System.out.println(stringBuilder.toString() + "\n" + stringBuilderCountry.toString());
	}

	/**
	 * @return the useWholeFileForTraining
	 */
	public boolean isUseWholeFileForTraining() {
		return useWholeFileForTraining;
	}

	/**
	 * @param useWholeFileForTraining the useWholeFileForTraining to set
	 */
	public void setUseWholeFileForTraining(boolean useWholeFileForTraining) {
		this.useWholeFileForTraining = useWholeFileForTraining;
	}
}
