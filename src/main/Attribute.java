package main;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Represents an attribute (column) read from the data file.
 * If the column has no name, a random name will be given.  
 */
public class Attribute {


	private String name; 				//name used to differentiate between attributes
	private ArrayList<Double> values; 	//all the values read from the data file, related to this column
	private double partition; 			//the number that separates the values into two ranges (>partition and <= partition)
	private double entropy1;			// <=
	private double entropy2;			// >
	private double informationGain;
	private boolean belongsToTree; 		//if the attribute is being used in the tree already
	
	/**
	 * Class default constructor. 
	 * @param name The name of this attribute
	 */
	public Attribute(String name) {
		this.name = name;
		this.values = new ArrayList<Double>();
		this.belongsToTree = false;
	}
	
	
	public double getMinValue() {
		return Collections.min(values);
	}
	
	public double getMaxValue() {
		return Collections.max(values);
	}
	
	public int getNumValues() {
		return values.size();
	}
	
	
	public ArrayList<Double> getValuesBelowOrEqual(double value) {
		ArrayList<Double> count = new ArrayList<Double>();
		
		for(int i = 0; i < values.size(); i++) {
			if(values.get(i) <= value) { 
				count.add(value);
			}
		}		
		
		
		return count;
	}
	
	public ArrayList<Double> getValuesAbove(double value) {
		ArrayList<Double> count = new ArrayList<Double>();
		
		for(int i = 0; i < values.size(); i++) {
			if(values.get(i) > value) 
				count.add(value);
		}		
		
		return count;
	}
	
	public ArrayList<Integer> getIndexesBelowOrEqual(double value) {
		ArrayList<Integer> count = new ArrayList<Integer>();
		
		for(int i = 0; i < values.size(); i++) {
			if(values.get(i) <= value) { 
				count.add(i);
			}
		}		
		
		
		return count;
	}
	
	public ArrayList<Integer> getIndexesAbove(double value) {
		ArrayList<Integer> count = new ArrayList<Integer>();
		
		for(int i = 0; i < values.size(); i++) {
			if(values.get(i) > value) 
				count.add(i);
		}		
		
		return count;
	}
	
	/**
	 * 
	 * @param value
	 * @return the number of values below of equal to value
	 */
	public int countValuesBelowOrEqual(double value) {
		int count = 0;
		
		for(Double v : values) {
			if(v <= value)
				count ++;
		}
		
		return count;
	}
	
	/**
	 * 
	 * @param value
	 * @return the number of values above the value passed as argument
	 */
	public int countValuesAbove(double value) {
		int count = 0;
		
		for(Double v : values) {
			if(v > value) 
				count++;
		}
		
		return count;
	}

	/**
	 * Adds a value to the array list of values.
	 * @param value the value to add to the array list.
	 */
	public void addValue(double value) {
		this.values.add(value);
	}
	
	/**
	 * Adds a value to the array list of values. 
	 * @param value the value to add to the array list, as a string.
	 */
	public void addValue(String value) {		
		values.add(Double.parseDouble(value));
	}
	
	/**
	 * @return the attribute's values
	 */
	public ArrayList<Double> getValues() {
		return values;
	}
	/**
	 * @param values the attribute's values to set
	 */
	public void setValues(ArrayList<Double> values) {
		this.values = values;
	}
	/**
	 * @return this attribute's name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the attribute's name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the partition
	 */
	public double getPartition() {
		return partition;
	}

	/**
	 * @param partition the partition to set
	 */
	public void setPartition(double partition) {
		this.partition = partition;
	}

	/**
	 * @return the entropy
	 */
	public double getEntropy1() {
		return entropy1;
	}

	/**
	 * @param entropy the entropy to set
	 */
	public void setEntropy1(double entropy) {
		this.entropy1 = entropy;
	}

	/**
	 * @return the entropy
	 */
	public double getEntropy2() {
		return entropy2;
	}

	/**
	 * @param entropy the entropy to set
	 */
	public void setEntropy2(double entropy) {
		this.entropy2 = entropy;
	}
	
	/**
	 * @return the informationGain
	 */
	public double getInformationGain() {
		return informationGain;
	}

	/**
	 * @param informationGain the informationGain to set
	 */
	public void setInformationGain(double informationGain) {
		this.informationGain = informationGain;
	}

	public void chooseInterval() {
		//Collections.sort(values);
		this.partition = (getMaxValue() + getMinValue())/2;
	}

	/**
	 * @return the belongsToTree
	 */
	public boolean isBelongsToTree() {
		return belongsToTree;
	}


	/**
	 * @param belongsToTree the belongsToTree to set
	 */
	public void setBelongsToTree(boolean belongsToTree) {
		this.belongsToTree = belongsToTree;
	}
	
}
