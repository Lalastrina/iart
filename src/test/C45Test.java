package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import main.Attribute;
import main.C45;
import main.Classification;
import main.Interface;
import main.Parser;

import org.junit.Test;

public class C45Test {

	private static final double DELTA = 1e-4;
	
	/****************
	 * TEST ENTROPY
	 ****************/
	
	@Test
	public void testInitialEntropy0() {
		ArrayList<Classification> classes = new ArrayList<Classification>();
		classes.add(new Classification("CaboVerde"));
		classes.add(new Classification("CaboVerde"));
		main.C45 c45 = new main.C45();
		c45.setClasses(classes);
		
		assertEquals(0, c45.calculateEntropy(), DELTA);
		
	}
	
	@Test
	public void testInitialEntropy1() {
		ArrayList<Classification> classes = new ArrayList<Classification>();
		classes.add(new Classification("CaboVerde"));
		classes.add(new Classification("Brasil"));
		main.C45 c45 = new main.C45();
		c45.setClasses(classes);
		
		assertEquals(1, c45.calculateEntropy(), DELTA);
		
	}
	
	@Test
	public void testInitialEntropyNot0() {
		ArrayList<Classification> classes = new ArrayList<Classification>();
		classes.add(new Classification("CaboVerde"));
		classes.add(new Classification("CaboVerde"));
		classes.add(new Classification("Brasil"));
		main.C45 c45 = new main.C45();
		c45.setClasses(classes);
		
		//probabilidade(CaboVerde) = 2/3
		//probabilidade(Brasil) = 1/3
		//entropia = -2/3 * log(2/3,2) -1/3 * log(1/3,2) = -2/3 * (-0.58) -1/3 * (-1.58)
		
		assertEquals(-0.6667 * (-0.5849) -0.3333 * (-1.5850), c45.calculateEntropy(), DELTA);
		
	}
	
	@Test
	public void testInitialEntropyCarExample() {
		ArrayList<Classification> classes = new ArrayList<Classification>();
		classes.add(new Classification("Bus"));
		classes.add(new Classification("Bus"));
		classes.add(new Classification("Train"));
		classes.add(new Classification("Bus"));
		classes.add(new Classification("Bus"));
		classes.add(new Classification("Train"));
		classes.add(new Classification("Train"));
		classes.add(new Classification("Car"));
		classes.add(new Classification("Car"));
		classes.add(new Classification("Car"));
		main.C45 c45 = new main.C45();
		c45.setClasses(classes);
		
		assertEquals(1.571, c45.calculateEntropy(), DELTA);	
	}
	
	/****************
	 * TEST INTERVALS
	 ****************/
	@Test
	public void testPartition() {
		
		String file = "bin/cidades_teste.txt";
		Parser p = new Parser(file);
		ArrayList<Attribute> attrs = new ArrayList<Attribute>();
		ArrayList<Classification> classes = new ArrayList<Classification>();	
		p.parseCitiesFile(attrs, classes);
		
		for(Attribute atr : attrs) {
			atr.chooseInterval();
		}
		
		assertEquals(4, attrs.get(0).getPartition(), DELTA);
		assertEquals(5.5, attrs.get(1).getPartition(), DELTA);
		assertEquals(5, attrs.get(2).getPartition(), DELTA);
		assertEquals(4.5, attrs.get(3).getPartition(), DELTA);
		
	}
	
	@Test
	public void testIntervals() {
		String file = "bin/cidades_teste.txt";
		Parser p = new Parser(file);
		ArrayList<Attribute> attrs = new ArrayList<Attribute>();
		ArrayList<Classification> classes = new ArrayList<Classification>();	
		p.parseCitiesFile(attrs, classes);
		main.C45 c45 = new main.C45();
		c45.setClasses(classes);
		c45.setAttributes(attrs);

		c45.prepareAttributes();
		ArrayList<Classification> interval1 = new ArrayList<Classification>();
		ArrayList<Classification> interval2 = new ArrayList<Classification>();
		
		//Pelo primeiro atributo, Se > 3 -> CaboVerde, senao Brasil.
		interval1 = c45.getClassesForAttribute(attrs.get(0), attrs.get(0).getPartition(), true);
		interval2 = c45.getClassesForAttribute(attrs.get(0), attrs.get(0).getPartition(), false);
		
		for(Classification c : interval1) {
			assertEquals("CaboVerde", c.getName());
		}
		
		for(Classification c : interval2) {
			assertEquals("Brasil", c.getName());
		}

		//Pelo segundo atributo, Se > 3 -> CaboVerde (unico), senao pode ser brasil ou caboverde
		interval1 = c45.getClassesForAttribute(attrs.get(1), attrs.get(1).getPartition(), true);
		interval2 = c45.getClassesForAttribute(attrs.get(1), attrs.get(1).getPartition(), false);
		
		for(Classification c : interval1) {
			assertEquals("CaboVerde", c.getName());
		}
		
		for(int i = 0; i < interval2.size(); i++) {
			if(i < 2 || i > 3)
				assertEquals("Brasil", interval2.get(i).getName());
			else
				assertEquals("CaboVerde", interval2.get(i).getName());
		}

	}
	
	/****************
	 * TEST INFORMATION GAIN
	 ****************/
	@Test
	public void testInfoGain() {
		String file = "bin/cidades_teste.txt";
		Parser p = new Parser(file);
		ArrayList<Attribute> attrs = new ArrayList<Attribute>();
		ArrayList<Classification> classes = new ArrayList<Classification>();	
		p.parseCitiesFile(attrs, classes);
		main.C45 c45 = new main.C45();
		c45.setClasses(classes);
		c45.setAttributes(attrs);

		c45.prepareAttributes();
		double mainEntropy = c45.calculateEntropy();
		//mainEntropy: -3/6 * log base 2 of 3/6 - 3/6 * log base 2 of 3/6 = 1.0
		//entropiaIntervalo1: - 1 * log base 2 of 1 = 0
		//entropiaIntervalo2: - 1 * log base 2 of 1 = 0
		//mainEntropy - (3/6 * entropiaIntervalo1 + 3/6 * entropiaIntervalo2) = (3/6 * 0) + (3/6 * 0) = 0
		assertEquals(1.0, c45.calcInfoGain(mainEntropy, c45.getAttributes().get(0)), DELTA);
		assertEquals(1.0, c45.calcInfoGain(mainEntropy, c45.getAttributes().get(3)), DELTA);
	}
	
	/****************
	 * TEST TREE
	 ****************/
	@Test
	public void testTree() {
		String file = "bin/cidades_teste.txt";
		Parser p = new Parser(file);
		ArrayList<Attribute> attrs = new ArrayList<Attribute>();
		ArrayList<Classification> classes = new ArrayList<Classification>();	
		p.parseCitiesFile(attrs, classes);
		C45 c45 = new C45();
		c45.setClasses(classes);
		c45.setAttributes(attrs);
		
		c45.doMagic();
		
		//has three nodes
		assertEquals(3, c45.getTree().getNodes().size());
		assertEquals("a0", c45.getTree().getNodes().get(0).getAttrName());
	}
}
