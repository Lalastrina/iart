ID3 em geral:
https://www.youtube.com/watch?v=wL9aogTuZw8

Como dividir os valores em intervalos para o C4.5:
http://www.saedsayad.com/supervised_binning.htm

C4.5 em geral:
http://www.saedsayad.com/decision_tree.htm

Exemplo do que vamos ter no final:
http://www2.cs.uregina.ca/~dbd/cs831/notes/ml/dtrees/c4.5/c4.5_prob1.html

Diferencas ID3 e C4.5
http://www.cis.temple.edu/~giorgio/cis587/readings/id3-c45.html

Super cool pdf
http://thesai.org/Downloads/SpecialIssueNo10/Paper_3-A_comparative_study_of_decision_tree_ID3_and_C4.5.pdf

Gain Ratio
http://slidewiki.org/slide/24156

Others:
http://arxiv.org/pdf/cs/9603103.pdf